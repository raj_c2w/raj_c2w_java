class Real{
	public static void main(String[]args){
		int x =0;
		System.out.println(++x + ++x + ++x + ++x); //1+2+3+4=10
		System.out.println(++x + x++ + x++ + x++); //5+5+6+7=23
		System.out.println(++x + x++ + ++x + ++x); //9+9+11+12=41
		System.out.println(--x + x-- + --x + --x); //11+11+9+8=39
		System.out.println(x-- + x-- + --x + --x); //8+7+5+4=24
	}
}
