import java.util.*;
class ArrayDemo{
	public static void main(String[]args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter Array Size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		for(int i = 0; i<arr.length;i++){
			System.out.print("Enter Element: ");
			arr[i]=sc.nextInt();
		}
		int sum = 0;
			for(int i = 0;i<arr.length;i++){
				sum+=arr[i];
			}
			System.out.println("Sum: "+sum);
	}
}


