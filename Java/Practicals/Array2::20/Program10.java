import java.util.*;
class Array{
        public static void main(String[]args){
                Scanner sc = new Scanner (System.in);
                        System.out.print("Enter Size: ");
                        int size=sc.nextInt();
                        int arr[]=new int[size];
                        System.out.println("Enter Elements: ");
			for(int i=0; i<size; i++){
				arr[i]=sc.nextInt();
			}
			int max=arr[0];
			int Position=0;
			for(int i=0; i<size; i++){
				if(max<arr[i]){
					max=arr[i];
					Position=i;
				}
			}
			System.out.print("Maximum number in the array is found at pos "+Position+" is "+max);
	}
}
