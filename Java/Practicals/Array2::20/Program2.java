import java.util.*;
class Array{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Size: ");
		int size = sc.nextInt();
		int arr[]=new int [size];
		int sum=0;
		System.out.println("Enter Elements: ");
		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
		for(int i=0; i<size; i++){
			if(arr[i]%3==0){
				sum+=arr[i];
			}
		}
		System.out.print("sum of elements divisible by 3 is: "+sum);
	}
}

