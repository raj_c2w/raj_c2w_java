import java.util.*;
class Array{
	public static void main(String[]args){
		Scanner sc = new Scanner (System.in);
			System.out.print("Enter Size: ");
			int size=sc.nextInt();
			int arr[]=new int[size];
			int Product=1;
			System.out.print("Enter Elements: ");
			for(int i=0; i<size; i++){
				arr[i]=sc.nextInt();
			}
			for(int i=0; i<size; i++){
				if(i%2==1){
					Product*=arr[i];
				}
		}
			System.out.print("Product of odd indexed elements: "+Product);
	}
}
