import java.io.*;
class Pattern{                                                                         public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));                                                                               System.out.print("Enter a Row: ");
                int Row = Integer.parseInt(br.readLine());
                        
                for(int i =1;i<=Row;i++){
			char ch1 ='A';
		        char ch2 = 'a';
			for(int j=Row;j>=i;j--){
				if(i%2==0){
					System.out.print(ch2+" ");
					ch2++;
				}else{
					System.out.print(ch1+" ");
					ch1++;
				}
			}
			System.out.println();
		}
}
}
