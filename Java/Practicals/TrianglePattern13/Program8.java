import java.io.*;
class Pattern{                                                                         public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));                                                                               System.out.print("Enter a Row: ");
                int Row = Integer.parseInt(br.readLine());
                        
                for(int i =1;i<=Row;i++){
			int num = Row-i+1;
			int ch = 65+Row-i;
			for(int j = Row;j>=i;j--){
				if(i%2==1){
					System.out.print(num+" ");
					num--;
				}else{
					System.out.print((char)ch+" ");
					ch--;
				}
			}
			System.out.println();
		}
}
}
