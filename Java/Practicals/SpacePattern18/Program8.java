import java.util.*;
class Space{
        public static void main(String[]args){
                Scanner sc = new Scanner(System.in);
                        System.out.print("Enter Row: ");
                        int Row = sc.nextInt();
			for(int i=1; i<=Row; i++){
				for(int space=1; space<i; space++){
					System.out.print(" "+" ");
				}
				int num=i;
				for(int j=Row; j>=i; j--){
					System.out.print(num+" ");
					num++;
				}
				System.out.println();
			}
	}
}
