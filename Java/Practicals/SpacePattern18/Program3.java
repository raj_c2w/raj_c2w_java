import java.util.Scanner;
class Space{
        public static void main(String[]args){
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter Row: ");
                int Row = sc.nextInt();
		for(int i=Row; i>=1; i--){
			for(int space=1; space<i; space++){
				System.out.print(" "+" ");
			}
			int ch = 64+i;
		       for(int j=Row; j>=i; j--){
		       		System.out.print((char)ch+" ");
			 	ch++;
		       }
		       System.out.println();
		}
	}
}
