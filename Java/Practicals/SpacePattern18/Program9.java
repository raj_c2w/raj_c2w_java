import java.util.*;
class Space{
        public static void main(String[]args){
                Scanner sc = new Scanner(System.in);
                        System.out.print("Enter Row: ");
                        int Row = sc.nextInt();
			for(int i=Row; i>=1; i--){
				for(int space=Row; space>i; space--){
					System.out.print(" "+" ");
				}
				int ch=64+Row;
				for(int j=1; j<=i; j++){
					System.out.print((char)ch+" ");
					ch--;
				}
				System.out.println();
			}
	}
}

