import java.io.*;
class Pattern{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row: ");
                int Row = Integer.parseInt(br.readLine());
		int ch = 64+Row;
		
		for(int i =1; i<=Row; i++){
			int num =Row+i-1;
			for(int j=1;j<=Row;j++){
				System.out.print((char)ch);
				System.out.print(num--+" ");
				
				
			}
			System.out.println();
			
		}
	}
}

