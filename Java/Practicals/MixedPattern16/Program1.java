import java.io.*;
class Pattern{
	public static void main(String[]args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Row: ");
		int Row = Integer.parseInt(br.readLine());
		int num = 1;
		for(int i = 1; i<=Row; i++){
			for(int j=1; j<=Row;j++){
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}

