import java.io.*;
class Pattern{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row: ");
                int Row = Integer.parseInt(br.readLine());
		for(int i =1; i<=Row;i++){
			int ch = 96+Row;
			int num = Row;
			for(int j = 1; j<=i;j++){
				if(i%2==0){
					System.out.print(num+" ");
					num--;
				}else{
					System.out.print((char)ch+" ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}
