import java.io.*;
class Pattern{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row: ");
                int Row = Integer.parseInt(br.readLine());
		for(int i=Row;i>=1;i--){
			int num = 1;
			for(int j=Row; j>=i; j--){
				System.out.print(i*num+" ");
				num++;

			}
			System.out.println();
		}
	}
}
