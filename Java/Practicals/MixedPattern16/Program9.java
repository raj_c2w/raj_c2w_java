import java.io.*;
class Pattern{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row: ");
                int Row = Integer.parseInt(br.readLine());
		for(int i = 1; i<=Row; i++){
			int num = 1;
			int ch = 65+Row-i;
			for(int j = Row; j>=i;j--){
				if(i%2==0){
				System.out.print((char)ch+" ");
			}else{
				System.out.print(num+" ");
				num++;
			}
			ch--;
		}
		System.out.println();
	}
}
}
