import java.io.*;
class ReverseSquare{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter a Number: ");
                long Num = Long.parseLong(br.readLine());
		long rem = 0;
		long rev = 0;
		while(Num>0){
			rem=Num%10;
			rev = rev*10+rem;
			Num/=10;		
		}
		while(rev>0){
			rem=rev%10;
			rev/=10;
		
			if(rem%2==1)
				System.out.print(rem*rem+" ");
		}
	}

}
