class Pattern{
	public static void main(String[]args){
		System.out.println("Number of rows = 3");
		int row = 3;
		char ch = 'A';
		for(int i = 1; i<=row; i++){
			for(int j=1; j<=row; j++){
				System.out.print(j + ch++ + " ");
			}
			System.out.println();
		}
	}
}
