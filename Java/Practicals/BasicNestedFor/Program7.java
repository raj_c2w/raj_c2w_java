class Pattern{
	public static void main(String[]args){
		System.out.println("Number of rows = 3");
		int row = 4;
		char ch = 'a';
		for (int i=1; i<=row;i++){
			for(int j= 1; j<=row;j++){
				System.out.print(ch-- +" ");
				ch ='a';
			}
			System.out.println();
		}
	}
}
