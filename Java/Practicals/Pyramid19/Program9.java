import java.util.*;
class Pyramid{
        public static void main(String[]args){
                Scanner sc = new Scanner(System.in);
                        System.out.print("Enter Row: ");
                        int Row = sc.nextInt();
			
                        for(int i=1; i<=Row; i++){
				char ch1='A';
				char ch2='a';
				for(int sp=Row; sp>i; sp--){
					System.out.print(" "+" ");
				}
				for(int j=1; j<=i*2-1; j++){
					if(i%2==1){
						if(j<i){
						System.out.print(ch1+" ");
						ch1++;
						}else{
							System.out.print(ch1+" ");
							ch1--;
						}
					}else{
						if(j<i){
							System.out.print(ch2+" ");
							ch2++;
						}else{
							System.out.print(ch2+" ");
							ch2--;
						}
					}
				}
				System.out.println();
			}
	}
}

