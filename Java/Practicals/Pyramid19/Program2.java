import java.util.*;
class Pyramid{
        public static void main(String[]args){
                Scanner sc = new Scanner(System.in);
                        System.out.print("Enter Row: ");
                        int Row = sc.nextInt();
			int num=1;
			for(int i=1; i<=Row; i++){
				for(int space=1; space<=Row-i; space++){
					System.out.print(" "+" ");
				}
				for(int j=1; j<=i*2-1; j++){
					System.out.print(num+" ");
					num++;
				}
				System.out.println();
			}
	}
}
