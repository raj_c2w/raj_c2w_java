import java.util.*;
class Pyramid{
        public static void main(String[]args){
                Scanner sc = new Scanner(System.in);
                        System.out.print("Enter Row: ");
                        int Row = sc.nextInt();
                        for(int i=1; i<=Row; i++){
				int num=1;
				for(int space=Row; space>i; space--){
					System.out.print(" "+" ");
				}
				for(int j=1; j<=i*2-1; j++){
					
					if(j<i){
						System.out.print(num+" ");
					num++;
					}else{
						System.out.print(num+" ");
						num--;
					}
				}
				System.out.println();
			}
	}
}
