import java.io.*;
class Palindrome{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter a Number: ");
                int num = Integer.parseInt(br.readLine());
                int rem = 0;
                int temp = num;
                int var = 0;
                while(num>0){
                        rem=num%10;
                        var=var*10+rem;
                        num/=10;
                }
                if(var==temp){
			System.out.print(temp+" is a palindrome number");
		}else{
			System.out.print(temp+" is not a palindrome number");

        }
}
}
