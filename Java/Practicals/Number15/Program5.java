import java.io.*;
class Factorial{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter a Number: ");
                int num = Integer.parseInt(br.readLine());
		int fact=1;
		int temp=num;
		while(num>=1){
			fact=fact*num;
			num--;
		}
		System.out.print("Factorial of "+temp+" is "+fact);
	}
}
