import java.io.*;
class Pattern{
	public static void main(String[]args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Row: ");
		int Row = Integer.parseInt(br.readLine());

		for(int i=1; i<=Row; i++){
			int ch = 64+Row;
			for(int j = Row; j<=i; j--){
				if(ch%2==1){
					System.out.print(ch+" ");
				}else{
					System.out.print((char)ch+" ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
