import java.io.*;
class Pattern{
	public static void main(String[]args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Row: ");
		int Row = Integer.parseInt(br.readLine());
		for(int i = 1; i<=Row; i++){
			int ch = Row+64;
			for(int j = Row; j>=i;j--){
				System.out.print((char)ch+" ");
				ch--;
			}
			System.out.println();
		}
	}
}
