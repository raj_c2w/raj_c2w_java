import java.io.*;
class Pattern{
        public static void main(String[]args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Row: ");
                int Row = Integer.parseInt(br.readLine());
                 char ch = 'a';
                for(int i = 1; i<=Row; i++){
                        int num = 1;

                        for(int j = 1; j<=i; j++){
                                if(j%2==1){
                                System.out.print(num+" ");
                                
                                }else{
                                        System.out.print(ch+" ");
                                }
				num++;
                                ch++;
                }
                System.out.println();
        }

}
}
